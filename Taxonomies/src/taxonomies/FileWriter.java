/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package taxonomies;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author monika
 */
public class FileWriter 
{
    static String code;
    static List<String> codeSet;
    static Map<Integer, String> codes;
    static Map<Integer, String> types;
    
    
    static public void saveAsNewick(String fTree, String fMap, Node root) throws IOException
    {
        codes = new HashMap<>();
        types = new HashMap<>();
        code = "";
        codeSet = new LinkedList<>();
        
        Map<String, Integer> rankIds = new HashMap<>();
        rankIds.put("no rank", 0);
        rankIds.put("domain", 127);
        rankIds.put("kingdom", 1);
        rankIds.put("genus", 98);
        rankIds.put("phylum", 2);
        rankIds.put("species group", 99);
        rankIds.put("class", 3);
        rankIds.put("species", 100);
        rankIds.put("order", 4);
        rankIds.put("subspecies", 101);
        rankIds.put("family", 5);
        rankIds.put("varietas", 90);
                
        getNewick(root);
        if(code.substring(code.length() - 1).contentEquals(","))
        {
            code = code.substring(0, code.length()-1);
        }
        code = code + ";";
        codeSet.add(code);
        
        BufferedWriter bw = new BufferedWriter(new java.io.FileWriter(fTree));
        while(!codeSet.isEmpty())
        {
            bw.append(codeSet.remove(0));
        }
        bw.newLine();
        bw.close();
        bw = new BufferedWriter(new java.io.FileWriter(fMap));
        for(Integer id1 : codes.keySet())
        {
            String name1 = codes.get(id1);
            String type1 = types.get(id1);
            if(type1 == null)
            {
                type1 = "no rank";
            }
            Integer rank = rankIds.get(type1);
            if(rank == null)
            {
                rank = 0;
            }
            bw.append(id1 + "\t" + name1 + "\t-1\t" + rank + "\n");
        }
        bw.close();
        
    }
    
    static private void getNewick(Node n)
    {
        if(code.length() > 1000)
        {
            codeSet.add(code);
            code = "";
        }
        if(!n.children.isEmpty())
        {
            code = code + "(";
            Node[] chi = new Node[n.children.size()];
            chi = n.children.values().toArray(chi);
            Arrays.sort(chi);
            
            if(chi[0].type.contentEquals("domain"))//isDomain(chi))
            {
                Node[] spec = new Node[chi.length];
                boolean[] taken = new boolean[spec.length];
                String[] order = new String[]{"bacteria","archaea","eukaryota","viruses"};
                int ind = 0;
                for(String o : order)
                {
                    for (int i = 0; i < spec.length; i++)
                    {
                        if(chi[i].name.contentEquals(o))
                        {
                            spec[ind++] = chi[i];
                            taken[i] = true;
                            break;
                        }
                    }
                }
                for (int i = 0; i < taken.length; i++) {
                    if(!taken[i])
                    {
                        spec[ind++] = chi[i];
                    }
                }
                chi = spec;
            }
            
            for(Node c : chi)
            {
                getNewick(c);
            }
            
            if(code.substring(code.length() - 1).contentEquals(","))
            {
                code = code.substring(0, code.length()-1);
            }
            
            code = code + ")";
            if(n.nameOriginal != null && n.nameOriginal.length() > 0)
            {
                code  = code + n.id;
                codes.put(n.id, n.nameOriginal);
                types.put(n.id, n.type);
            }
            code = code + ",";
        }
        else
        {
            if(n.nameOriginal != null && n.nameOriginal.length() > 0)
            {
                code = code + n.id;
                codes.put(n.id, n.nameOriginal);
                types.put(n.id, n.type);
            }
            code = code + ",";
        }
    }
    
    static public void writeInfoFile(String fileInfo, String type) throws IOException
    {
        
        BufferedWriter bw = new BufferedWriter(new java.io.FileWriter(fileInfo));
        bw.append(Info.citations.get(type));
        bw.close();
    }

    static void writeBiom(JSONObject biomFile, File file) throws IOException 
    {
        BufferedWriter bw = new BufferedWriter(new java.io.FileWriter(file));
        bw.write(biomFile.toJSONString());
        bw.flush();
        bw.close();

    }
}
