/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package taxonomies;

import com.itextpdf.text.DocumentException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author monika
 */
public class Taxonomies 
{
    /**
     * @param args the command line arguments
     */
    
    static Parameters par;
    
    static Node treeToMap = null;
    
    static Map<String,ArrayList<Node>> ref;
    static Node refTree = null;
    
    static Info info = new Info();
    
    static ArrayList<Node> biomNodes;
    static ArrayList<JSONObject> jsonObjects;
    
    static JSONObject biomFile;
    
    static File inputFile;
    static String refTax;
    static File outputFile;
    static Boolean nwk;
    static File hmFile;
    static File psFile;
    static Boolean synonims;
    static Boolean help;
    static Boolean printValues;
    static Boolean strict;
    static Boolean admin;
    
    static Boolean map;
    static String inTax;
    static Boolean paths;
    
    static boolean errors;
    
    public static void main(String[] args) throws Exception 
    {
        try
        {
            parseParameters(args);
        }
        catch(Exception ex)
        {
            reportAnError(ex.getMessage());
        }
        
        if(!errors && !help)
        {
            if(inputFile != null)
            {
                try
                {
                    parseFilesBiom(true);
                    
                    if(strict)
                    {
                        removeIntermediateNodes();
                        Mapper.mapStrict(treeToMap, refTree);
                    }
                    else
                    {
                        Mapper.mapLoose(treeToMap, ref);
                    }
                     
                    Mapper.replaceTaxonomiesInBiom(biomNodes, jsonObjects);
                    saveBiomFile();
                    evaluateMapping();
                }
                catch (Exception ex)
                {
                    System.err.println(ex.getMessage());
                    ex.printStackTrace();
                    errors = true;
                }
            }
            else if(map == true && inTax != null)
            {
                parseFiles(strict);
                removeIntermediateNodes();
                if(strict)
                {
                    //removeIntermediateNodes();
                    Mapper.mapStrict(treeToMap, refTree);
                }
                else
                {
                    Mapper.mapLoose(treeToMap, ref);
                }
                
                evaluateMapping();
                
            }
            else
            {
                parseSynonims();
                ref = new HashMap<>();
                parseReference();
            }
            
            if(nwk && !errors)
            {
                String refTre = refTax + ".tre";
                String refMap = refTax + ".map";
                String refInfo = refTax + ".info";
                
                try
                {
                    FileWriter.saveAsNewick(refTre, refMap, refTree);
                }
                catch (IOException ioe)
                {
                    System.err.println("Error occured while saving tree.");
                }

                try
                {
                    FileWriter.writeInfoFile(refInfo, refTax);
                }
                catch (IOException ioe)
                {
                    System.err.println("Error occured while saving .info file.");
                }
            }
        }
    }

    private static void saveBiomFile() throws Exception 
    {
        try 
        {
            FileWriter.writeBiom(biomFile, outputFile);
        } 
        catch (IOException ioe) 
        {
            throw new Exception("Error writing BIOM file to the output.");
        }
    }

    private static void removeIntermediateNodes() 
    {
        Mapper.removeIntermediate(null, treeToMap);
        Mapper.removeIntermediate(ref, refTree);
    }

    
    private static boolean evaluateMapping() 
    {
        Evaluator ev = new Evaluator();
        try 
        {
            ev.makeStats(treeToMap, refTree, null, psFile, hmFile, paths);
        } 
        catch (DocumentException | IOException de) 
        {
            System.err.println("Error while saving mapping stats.");
            return true;
        }
        return false;
    }


    private static void parseFiles(boolean makeMap) throws Exception 
    {
        boolean parsed = false;
        if(makeMap)
        {
            ref = new HashMap<>();
        }
        ref = new HashMap<>();
        if (synonims) 
        {
            if (refTax.contentEquals("ncbi")) 
            {
                parseReference();
                parseInput();
                parsed = true;
            } 
            else if (!inTax.contentEquals("ncbi")) 
            {
                parseSynonims();
            }
        }
        if (!parsed) 
        {
            parseInput();
            parseReference();
        }
    }
    
    
    private static boolean parseFilesBiom(boolean makeMap) throws Exception 
    {
        
        boolean parsed = false;
        if(makeMap)
        {
            ref = new HashMap<>();
        }
        ref = new HashMap<>();
        if (synonims) 
        {
            if (refTax.contentEquals("ncbi")) 
            {
                parseReference();
                parseInputBiom();
                parsed = true;
            } 
            else
            {
                parseSynonims();
            }
        }
        if (!parsed) 
        {
            parseInputBiom();
            parseReference();
        }
        return false;
    }

    private static void parseSynonims() throws Exception 
    {
        try 
        {
            FileParser.parseNCBISynonimsOnly();
        } 
        catch (IOException ex) 
        {
            throw new Exception("Error occured while parsing names file.");
        }
    }

    private static void parseReference() throws Exception 
    {
        try 
        {
            refTree = FileParser.parseReference(refTax, synonims, ref);
        } 
        catch (IOException ex) 
        {
            ex.printStackTrace();
            throw new Exception("Error occured while parsing reference taxonomy.");
        }
    }

    private static void parseInput() throws Exception 
    {
        try 
        {
            treeToMap = FileParser.parseInput(inTax, synonims, null);
        } 
        catch (IOException ex) 
        {
           throw new Exception("Error occured while parsing input taxonomy.");
        }
    }
    
    private static void parseInputBiom() throws Exception
    {   
        try 
        {
            biomNodes = new ArrayList<>();
            jsonObjects = new ArrayList<>();
            treeToMap = new Node("root", "root", "BIOM");
            biomFile = FileParser.parseBiom(inputFile,biomNodes, jsonObjects, treeToMap);
        } 
        catch (IOException | ParseException  ex) 
        {
            throw new Exception("Error occured while parsing input taxonomy.");
        }
    }

    private static void parseParameters(String[] args) throws Exception 
    {
        Parameters.args = args;
        inputFile = Parameters.getFile("-i","--in","input BIOM file.", null, true, false);
        refTax = Parameters.getString("-r","--reference", 
                "reference taxonomy [ncbi/rdp/silva/greengenes/ott].",
                "ncbi", true, false);
        outputFile = Parameters.getFile("-o", "--out", "output BIOM file.", null, true, false);
        synonims = Parameters.getBoolean("-d", "--dictionary", "use synonim dictionary "
                + "as provided by ncbi.",  false, true, false);
        strict = Parameters.getBoolean("-s", "--strict", "performs strict mapping"
                + " (default mapping procedure is loose).", false, true, false);
        
        nwk = Parameters.getBoolean("-n","--nwk", 
                "print reference taxonomy as a newick file."
                + "Creates files <taxonomy>.tre and <taxonomy>.map for MEGAN.",
                false, true, false);
        hmFile = Parameters.getFile("-hm", "--heatmap", "heatmap file. "
                + "Saves summary of the cross mapping as a heatmap in a pdf file.",
                null, true, false);
        psFile = Parameters.getFile("-ps", "--parallelsets", "parallel sets file. "
                + "Saves summary of the cross mapping as a parallel sets diagram"
                + " in a pdf file.",
                null, true, false);
       
        
        inTax = Parameters.getString("-t","--intaxonomy", 
                "input taxonomy [ncbi/rdp/silva/greengenes/ott].",
                null, false, true);
        map = Parameters.getBoolean("-m", "--map", "maps whole taxonomy."
                + " Requires input taxonomy to be specified." , false, false, true);
        paths = Parameters.getBoolean("-p", "--paths", "maps paths in whole taxonomy."
                + " Requires input taxonomy to be specified." , false, false, true);
        admin = Parameters.getBoolean("-a", "--admin", "unlocks extra features", false, false, true);
        
        
        printValues = Parameters.getBoolean("-v", "--values",
                "prints values of all parameters as set by the user or by default.", false, false, false);
        
        help = Parameters.getBoolean("-h", "--help", "prints this message.", false, false, false);
         
        if(!admin)
        {
            if(inputFile == null && nwk == false && !help)
            {
                reportAnError("Nothing to do.");
            }
            if(inputFile!= null && outputFile == null)
            {
                reportAnError("Missing output file.");
            }
            if(inputFile== null && outputFile != null)
            {
                reportAnError("Missing input file.");
            }
        }
        else
        {
            if(inputFile == null && nwk == false && map == false && !help)
            {
                 reportAnError("Nothing to do.");
            }
            if(inputFile!= null && outputFile == null)
            {
                reportAnError("Missing output file.");
            }
            if(inputFile!= null && outputFile == null)
            {
                reportAnError("Missing output file.");
            }
            if(inputFile != null && map == true && inTax != null)
            {
                reportAnError("Too many parameters.");
            }
        }
        
        
        
        if(printValues)
        {
            Parameters.printValues();
        }
        
        if(help)
        {
            Parameters.printHelp();
        }
        
    }
    
    private static void reportAnError(String text)
    {
        System.err.println("Error: " + text);
        errors = true;
    }
    
}
