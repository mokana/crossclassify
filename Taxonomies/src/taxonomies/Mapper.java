/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package taxonomies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author monika
 */
public class Mapper 
{
    static void mapLoose(Node root, Map<String, ArrayList<Node>> ref) 
    {
        ArrayList<Node> nodesToMap = new ArrayList<>();
        nodesToMap.add(root);
        
        while(!nodesToMap.isEmpty())
        {
            Node n = nodesToMap.remove(0);
            String domain = n.getDomain();
            nodesToMap.addAll(n.children.values());
            mapANode(n, ref);
        }
    }

    private static void mapANode(Node n, Map<String, ArrayList<Node>> ref)
    {
        ArrayList<Node> candidates = new ArrayList<>();
        
        candidates = addCandidates(n, candidates, ref);
        
        Node best = bestCandidate(n, candidates);
        if(best != null)
        {
            n.setM(best);
            n.addToPath();
        }
        else
        {
            n.setM(n.parent.getM());
        }
    }
    
    private static ArrayList<Node> addCandidates(Node n, 
            ArrayList<Node> candidates,Map<String, ArrayList<Node>> ref)
    {
        String key = n.name + "-" + n.type;
        if(ref.containsKey(key))
        {
            candidates.addAll(ref.get(key));
        }
        if(n.type.contentEquals("species"))
        {
            key = n.parent.name + " " + n.name + "-" + n.type;
            if(ref.containsKey(key))
            {
                candidates.addAll(ref.get(key));
            }
            key = n.name + " sp.-" + n.type;
            if(ref.containsKey(key))
            {
                candidates.addAll(ref.get(key));
            }
        }
        return candidates;
    }
    
    private static Node bestCandidate(Node n, ArrayList<Node> candidates)
    {
        Node bestMatch = null;
        double bestScore = 0.0;
        
        if(candidates.size() == 1)
        {
            bestMatch = candidates.iterator().next();
            return bestMatch;
        }
        else
        {
            ArrayList<String> nPath = n.getPathSet();

            for(Node c: candidates)
            {
                ArrayList<String> p = c.getPathSet();
                
                int score = 0;
                for(String pn : nPath)
                {
                    if(p.contains(pn))
                    {
                        score++;
                    }
                }
                
                double sc = ((double) score) / Math.max((double) nPath.size(), (double) p.size());
                
                if(sc > bestScore)
                {
                    bestScore = sc;
                    bestMatch = c;
                }
                else if(sc == bestScore)
                {
                    if(c.type.contentEquals("species") 
                            || (c.parent!= null && c.parent.type.contentEquals("species")))
                    {
                        bestScore = sc;
                        bestMatch = c;
                    }
                    else if(bestMatch.type.contentEquals("species")
                            || (bestMatch.parent!= null && bestMatch.parent.type.contentEquals("species"))){}
                    else if(bestMatch.getPath().size() > c.getPath().size())
                    {
                        bestScore = sc;
                        bestMatch = c;
                    }
                }
                if(sc == 1.0)
                {
                    break;
                }
            }
        }
        return bestMatch;
    }
    
    public static void removeIntermediate(Map<String, ArrayList<Node>> map, Node root)
    {
        if(root != null)
        {
            ArrayList<Node> nodes = new ArrayList<>();
            nodes.add(root);
            while(!nodes.isEmpty())
            {
                Node n = nodes.remove(0);
                nodes.addAll(n.children.values());
                if(!Info.hi.containsKey(n.type))
                {
                    if(n.parent != null)
                    {
                        if(map != null)
                        {
                            String key = n.generateKey();
                            ArrayList<Node> value = map.get(key);
                            value.remove(n);
                            map.put(key, value);
                        }
                        Node p = n.parent;
                        p.children.remove(n.name);
                        for(Node c : n.children.values())
                        {
                            c.parent = p;
                            p.putChild(c.name, c);
                        }
                    }
                }
            }
        }
    }

    static void mapStrict(Node treeToMap, Node refTree) 
    {
        if(treeToMap.name.contentEquals(refTree.name) 
                && treeToMap.type.contentEquals(refTree.type))
        {
            treeToMap.setM(refTree);
            for(Node n1 : treeToMap.children.values())
            {
                Node match = null;
                for(Node m1 : refTree.children.values())
                {
                    if(m1.name.contentEquals(n1.name) && m1.type.contentEquals(n1.type))
                    {
                        match = m1;
                    }
                }
                if(match != null)
                {
                    mapStrict(n1, match);
                }
                else
                {
                    ArrayList<Node> toM = new ArrayList<>();
                    toM.add(n1);
                    while(!toM.isEmpty())
                    {
                        Node a = toM.remove(0);
                        a.setM(refTree);
                        toM.addAll(a.children.values());
                    }
                }
            }
        }
    }

    static void replaceTaxonomiesInBiom(ArrayList<Node> biomNodes, ArrayList<JSONObject> jsonObjects) 
    {
        for (int i = 0; i < biomNodes.size(); i++) 
        {
            JSONObject jo = jsonObjects.get(i);
            JSONObject metadata = (JSONObject) jo.get("metadata");
            //JSONArray taxonomy = (JSONArray) metadata.get("taxonomy");
            
            Node node = biomNodes.get(i);
            String[] taxonomy = newTaxonomy();
            LinkedList<Node> path = node.getM().getPath();
            for(Node n : path)
            {
                switch(n.type)
                {
                    case "domain" : taxonomy[0] += n.nameOriginal; break;
                    case "phylum" : taxonomy[1] += n.nameOriginal; break;
                    case "class" :  taxonomy[2] += n.nameOriginal; break;
                    case "order" :  taxonomy[3] += n.nameOriginal; break;
                    case "family" : taxonomy[4] += n.nameOriginal; break;
                    case "genus" :  taxonomy[5] += n.nameOriginal; break;
                    case "species" :taxonomy[6] += n.nameOriginal; break;
                }
            }
            JSONArray ja = new JSONArray();
            ja.addAll(Arrays.asList(taxonomy));
            metadata.put("taxonomy", ja);
        }
    }

    private static String[] newTaxonomy() 
    {
        String[] taxonomy = new String[7];
        taxonomy[0] = "k__";
        taxonomy[1] = "p__";
        taxonomy[2] = "c__";
        taxonomy[3] = "o__";
        taxonomy[4] = "f__";
        taxonomy[5] = "g__";
        taxonomy[6] = "s__";
        return taxonomy;
    }
}
