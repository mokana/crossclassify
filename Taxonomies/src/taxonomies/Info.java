/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package taxonomies;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author monika
 */
public class Info 
{
    static Map<String, Integer> hi;
    static Map<Integer, String> ih;
    static Map<String, String> citations;
    
    public Info()
    {
        hi = new HashMap<>();
        ih = new HashMap<>();
        citations = new HashMap<>();
        
        hi.put("root", 0);
        hi.put("domain", 1);
        hi.put("phylum", 2);
        hi.put("class", 3);
        hi.put("order", 4);
        hi.put("family", 5);
        hi.put("genus", 6);
        hi.put("species", 7);
        
        ih.put(0, "root");
        ih.put(1, "domain");
        ih.put(2, "phylum");
        ih.put(3, "class");
        ih.put(4, "order");
        ih.put(5, "family");
        ih.put(6, "genus");
        ih.put(7, "species");
        
        citations.put("ncbi", "created: Oct 05 2016\n"
                + "cite: Federhen S (2012) NAR 40 D136–D143."); //NCBI
        citations.put("rdp",  "created: Sep 30 2016\n"
                + "cite: Cole J R et al (2014) NAR 42 D633-D642."); //RDP
        citations.put("silva",  "created: Sep 29 2016\n"
                + "cite: Quast C et al (2013) NAR 41 D590-D596. Yilmaz P et al (2014) NAR 42 D643-D648."); //SILVA
        citations.put("gg",  "created: May 05 2013\n"
                + "cite: McDonald D et al (2012) ISME 6(3) 610-618."); //GG
        citations.put("ott", "created: Sep 10 2013\n"
                + "cite: Hinchliff C E et al (2015) PNAS 112.41 12764-12769."); //OTT
       
    }
}
