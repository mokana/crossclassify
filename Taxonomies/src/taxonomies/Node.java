/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package taxonomies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author monika
 */
public class Node  implements Comparable<Node>, Serializable
{

    String nameOriginal;
    String type;
    String name;
    String source;
    int id;
    int parentId;
    Node parent;
    Map<String,Node> children;
    
    boolean print;
    Node m;
    int mappedTo;
    boolean[] mapped = new boolean[8];
        
    boolean inPath; //relevant for path mapping only

    Node(int pId) 
    {
        this.parentId = pId;
        init();
    }

    Node(int id, int pId, String nam, String rank, String source) 
    {
        this.id = id;
        this.parentId = pId;
        this.name = nam;
        this.type = rank;
        this.source = source;
        init();
    }

    Node(String name, String rank, String source) 
    {
        this.name = name;
        this.type = rank;
        this.source = source;
        init();
    }
    
    public Node(int id, int parent, String rank, String source)
    {
        this.id = id;
        this.parentId = parent;
        this.type = rank;
        this.source = source;
        init();
    }
    
    private void init()
    {
        children = new HashMap<>();
    }

    @Override
    public int compareTo(Node o) 
    {
        return nameOriginal.compareTo(o.nameOriginal);    
    }
    
    LinkedList<Node> getPath()
    {
        LinkedList<Node> path = new LinkedList();
        Node n = this;
        path.add(n);
        while(n.parent != null)
        {
            n = n.parent;
            path.add(0, n);
        }
        return path;
    }
    
    ArrayList<String> getPathSet()
    {
        ArrayList<String> set = new ArrayList<>();
        Node n = this;
        set.add(n.name+"-"+n.type);
        while(n.parent != null)
        {
            n = n.parent;
            set.add(n.name+"-"+n.type);
        }
        return set;
    }
    
    void setM(Node m)
    {
        print = true;
        this.m = m;
    }
    
    Node getM()
    {
        if(m == null)
        {
            return parent.getM();
        }
        return m;
    }

    String generateKey() 
    {
        return name + "-" + type;
    }

    void addToPath() 
    {
        if(!inPath)
        {
            inPath = true;
            if(parent != null)
            {
                parent.addToPath();
            }
        }
    }

    void putChild(String name, Node node) 
    {
        Node n = children.put(name, node);
        if(n != null)
        {
            //System.out.println(n.nameOriginal + "\t" + n.type +" -> " + node.nameOriginal + "\t" + node.type);
            //System.out.println(n == node);
        }
        
    }
    
    String getDomain()
    {
        if(type.contentEquals("domain"))
        {
            return name;
        }
        else if(parent == null)
        {
            return null;
        }
        else
        {
            return parent.getDomain();
        }
    }
}
