/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package taxonomies;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author monika
 */
public class FileParser 
{
    static InputStream IS;
    
    static Map<String,String> synonimsPhylum = new HashMap<>();
    static Map<String,String> synonimsClass = new HashMap<>();
    static Map<String,String> synonimsOrder = new HashMap<>();
    static Map<String,String> synonimsFamily = new HashMap<>();
    static Map<String,String> synonimsGenus = new HashMap<>();
    static Map<String,String> synonimsSpecies = new HashMap<>();
    
    static Map<String, ArrayList<Node>> allByName = new HashMap<>();

    static Node[] idMap = new Node[2000000];
    
    static ArrayList<Integer> list = null;
    
    static String nodesFile = "/data/nodes.dmp";
    static String namesFile = "/data/names.dmp";
    static String rdpFile = "/data/rdp.txt";
    static String silvaFile = "/data/silva.txt";
    static String ggFile = "/data/gg.txt";
    static String ottFile = "/data/ott.txt";
    
    
    static Node parseInput(String type, Boolean synonyms, Map<String, ArrayList<Node>> toMap) throws FileNotFoundException, IOException 
    {
        return parse(type, toMap, synonyms);
    }

    static Node parseReference(String type, Boolean synonyms, Map<String, ArrayList<Node>> ref) throws FileNotFoundException, IOException 
    {
        return parse(type, ref, synonyms);
    }

    static Node parse(String type,
            Map<String, ArrayList<Node>> map,
            Boolean synonyms) throws FileNotFoundException, IOException
    {
        switch (type)
        {
            case("ncbi"):
            {
                return parseNCBI(nodesFile, namesFile, map, synonyms);
            }
            case("rdp"):
            {
                return parseRDP(rdpFile, map);
            }
            case("gg"):
            {
                return parseGG(ggFile, map);
            }
            case("silva"):
            {
                return parseSILVA(silvaFile, map);
            }
            case("ott"):
            {
                return parseOTT(ottFile, map);
            }
            default:
                return null;
        }
    }

    private static Node parseNCBI(String nodes, String names, 
            Map<String, ArrayList<Node>> map, boolean createDictionary) throws FileNotFoundException, IOException 
    {
        ArrayList<String>[] nodeNames = null;
        String[] scNames = null;
        
        if(createDictionary)
        {
            nodeNames = new ArrayList[2000000];
            scNames = new String[2000000];
        }
        
        Map<String, Integer> types = new HashMap<>();
        
        //IS = new FileInputStream(
        IS = FileParser.class.getResourceAsStream(nodes);
        
        int last = 0;
        
        byte[] b = new byte[IS.available()];
        byte[] leftOver = null;
        
        int lastRead = 0;
        
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] >= 65 && b[i] <= 90)
                {
                    b[i] += 32;
                }
                if(b[i] == 10) // new line
                {
                    int id = 0;
                    int pId = 0;
                    String rank = "";
                    int bars = 0;
                    boolean gap = false;
                    for(int j = last; j < i; j++)
                    {
                        if(b[j] == 124) // separator "|" found
                        {
                            bars++;
                        }
                        else
                        {
                            if(bars == 0 && b[j] >= 48 && b[j] <= 57) //Reading id
                            {
                                id = id*10 + b[j] - 48;
                            }
                            if(bars == 1 && b[j] >= 48 && b[j] <= 57) //Reading parent id
                            {
                                pId = pId*10 + b[j] - 48;
                            }
                            if(bars == 2 && b[j] >= 97 && b[j] <= 122)
                            {
                                if(gap)
                                {
                                    rank += " ";
                                    gap = false;
                                }
                                rank += (char) b[j];
                            }
                            if(bars == 2 && b[j] <= 32 && rank.length() > 0)
                            {
                                gap = true;
                            }
                            if(bars == 3)
                            {
                                addNodeNCBI(rank, types, id, pId);
                                break;
                            }
                        }
                    }
                    last = i;
                }
            }
        }
        IS.close();
        
        //IS = new FileInputStream(names);
        IS = Taxonomies.class.getResourceAsStream(names);
        last = 0;
        
        b = new byte[IS.available()];
        leftOver = null;
        
        lastRead = 0;
        
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] >= 65 && b[i] <= 90)
                {
                    b[i] += 32;
                }
                if(b[i] == 10) // new line
                {
                    int id = 0;
                    String name = "";
                    String nameType = "";
                    int bars = 0;
                    boolean gap = false;
                    for(int j = last; j < i; j++)
                    {
                        if(b[j] == 124) // separator "|" found
                        {
                            bars++;
                        }
                        else
                        {
                            if(bars == 0 && b[j] >= 48 && b[j] <= 57) //Reading id
                            {
                                id = id*10 + (b[j] - 48);
                            }
                            if(bars == 1 && b[j] > 32)
                            {
                                //if(b[j] != 34 && b[j] != 91 && b[j] != 93 )
                                if(b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93 )
                                {
                                    if(gap && name.length() > 0)
                                    {
                                        name += " ";
                                    }
                                    gap = false;
                                    name += (char) b[j];
                                }
                            }
                            if(bars == 3 && b[j] > 32)
                            {
                                if(b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93 )
                                {
                                    if(gap && nameType.length() > 0)
                                    {
                                        nameType += " ";
                                    }
                                    gap = false;
                                    nameType += (char) b[j];
                                }
                            }
                            if(b[j] <= 32)
                            {
                                gap = true;
                            }
                        }
                        
                    }
                    assignNameToNode(nameType, id, name, map, scNames, 
                            nodeNames, list, createDictionary);
                    last = i;
                }
            }
        }
        IS.close();
        
        if(createDictionary)
        {
            makeSynonimDictionary(scNames, nodeNames);
        }
        
        Node root = idMap[1];
        
        if(map != null) //making sure that root rank is "named" in the same way
        {
            String rootKey = root.generateKey();
            ArrayList<Node> rootList = map.remove(rootKey);
            root.type = "root";
            root.name = "root";   
            map.put(root.generateKey(), rootList);
        }
        else
        {
            root.type = "root";
            root.name = "root";     
        }
        
        return root;        
    }
    
    //Parses NCBI names file to create a synonim dictionary
    protected static void parseNCBISynonimsOnly() throws FileNotFoundException, IOException 
    {
        parseNCBI(nodesFile, namesFile, null, true);
    }
    
    private static void addNodeNCBI(String rank, Map<String, Integer> types, int id, int pId) 
    {
        if(rank.contentEquals("superkingdom"))
        {
            rank = "domain";
        }
        
        int no = 1;
        if(types.containsKey(rank))
        {
            no += types.get(rank);
        }
        types.put(rank, no);
        
        Node node = new Node(id, pId, rank, "NCBI");
        idMap[id] =  node;
    }
    
    private static void makeSynonimDictionary(String[] scNames, ArrayList<String>[] nodeNames)
    {
        for(int id = 0; id < scNames.length; id++)
        {
            String scName = scNames[id];
            if(scName != null)
            {
                String type = idMap[id].type;

                putToSynonims(type, scName, scName);
                ArrayList<String> syn = nodeNames[id];
                if(syn != null)
                {
                    for(String s : syn)
                    {
                        putToSynonims(type, s, scName);
                    }
                }
            }
        }
    }
    
    private static void putToSynonims(String type, String name, String sc) 
    {
        switch (type) 
        {
            case "phylum":
                synonimsPhylum.put(name, sc);
                break;
            case "class":
                synonimsClass.put(name, sc);
                break;
            case "order":
                synonimsOrder.put(name, sc);
                break;
            case "family":
                synonimsFamily.put(name, sc);
                break;
            case "genus":
                synonimsGenus.put(name, sc);
                break;
            case "species":
                synonimsSpecies.put(name, sc);
                break;
        }
    }
    
    private static void assignNameToNode(String nameType, int id, String name, 
            Map<String, ArrayList<Node>> map, String[] scNames,
            ArrayList<String>[] nodeNames, ArrayList<Integer> list,
            boolean createDictionary) 
    {
        if(nameType.equals("scientific name"))
        {
            Node node = idMap[id];
            node.name = name;
            node.nameOriginal = name;
            
            if((list != null && list.contains(id) )|| list == null)
            {
                addToNameSet(map, node);
            }
            if(createDictionary)
            {
                scNames[id] = name;
            }
            
            
            ArrayList<Node> thisName = allByName.get(name);
            if(thisName == null)
            {
                thisName = new ArrayList<>();
            }
            thisName.add(node);
            allByName.put(name, thisName);
            
            if(node.id != node.parentId)
            {
                Node parent = idMap[node.parentId];
                node.parent = parent;
                parent.putChild(node.name, node);
            }
        }
        else if(createDictionary)
        {
            if(nodeNames[id] == null)
            {
                nodeNames[id] = new ArrayList<>();
            }
            nodeNames[id].add(name);
        }
    }

    private static Node parseRDP(String file, Map<String, ArrayList<Node>> map) throws FileNotFoundException, IOException 
    {
        int id = 1;
        Node start = new Node("root", "root", "RDP");
        start.id = id;
        start.nameOriginal = "RDP";
        addToNameSet(map, start);
        
        IS = FileParser.class.getResourceAsStream(file);
        int last = 0;
        
        byte[] b = new byte[IS.available()];
        byte[] leftOver = null;
        
        int lastRead = 0;
                
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] == 10) // new line
                {
                    //id = 0;
                    String[] el = new String[20];
                    String[] elLc = new String[20];
                    int index = 0;
                    String name = "";
                    String nameLc = "";
                    String rank;
                    boolean gap = false;
                    for(int j = last; j < i; j++)
                    {
                        if(b[j] == 59) // separator "tab" found
                        {
                            elLc[index] = nameLc;
                            el[index++] = name;
                            name = "";
                            nameLc = "";
                        }
                        else
                        {
                            if(b[j] > 32 && b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93 )
                            {
                                if(gap && name.length() > 0)
                                {
                                    name += " ";
                                    nameLc += " ";
                                }
                                gap = false;
                                name += (char) b[j];
                                if(b[j] >= 65 && b[j] <= 90)
                                {
                                    nameLc += (char) (b[j] + 32);
                                }
                                else
                                {
                                    nameLc += (char) b[j];
                                }
                            }
                            if(b[j] <= 32)
                            {
                                gap = true;
                            }
                        }
                    }
                    elLc[index] = nameLc;
                    el[index++] = name;
                    
                    Node previous = start;
                    
                    for (int k = 2; k < el.length; k += 2)
                    {
                        if(el[k] == null)
                        {
                            break;
                        }
                        if(el[k+1] == null || el[k+1].length() == 0)
                        {
                            rank = el[k-1];
                        }
                        else
                        {
                            rank = el[k+1];
                        }
                        name = getScName(elLc[k], rank);

                        Node n = previous.children.get(name);
                        
                        if(n == null)
                        {
                            n = new Node(name, rank, "RDP");
                            n.nameOriginal = el[k];
                            n.id = ++id;

                            addToNameSet(map, n);
                            previous.putChild(n.name, n);
                            n.parent = previous;
                        }
                        previous = n;
                    }
                    last = i;
                }
            }
        }
        IS.close();
        
        return start;
    }

    private static Node parseGG(String file, Map<String, ArrayList<Node>> map) throws FileNotFoundException, IOException 
    {
        int index = 1;
        HashMap<String, String> tax = new HashMap<>();
        tax.put("k", "domain");
        tax.put("p", "phylum");
        tax.put("c", "class");
        tax.put("o", "order");
        tax.put("f", "family");
        tax.put("g", "genus");
        tax.put("s", "species");
        
        Node root = new Node("root", "root", "GG");
        root.nameOriginal = "GREENGENES";
        root.id = index++;
        addToNameSet(map, root);
        
        IS = FileParser.class.getResourceAsStream(file);
        int last = 0;
        
        byte[] b = new byte[IS.available()];
        byte[] leftOver = null;
        
        int lastRead = 0;
        
        int genus = 0;
        
        HashMap<String,Integer> rankss = new HashMap<>();
        
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] == 10) // new line
                {
                    boolean indexIncreased = false;
                    boolean end = false;
                    int id = 0;
                    String name = "";
                    String rank = "";
                    Node previous = root;
                    int bars = 0;
                    for(int j = last; j <= i; j++)
                    {
                        //System.out.println(rank + "\t*\t" + name + "\t*\t" + id);
                        if(!end)
                        {
                            if(b[j] == 9 || b[j] == 32) // separator "tab" or "space" found
                            {
                                bars++;
                                while(b[j+1] == 9)
                                {
                                    j++;
                                }
                            }
                            else
                            {
                                if(bars == 0 && b[j] >= 48 && b[j] <= 57) //Reading id
                                {
                                    id = id*10 + (b[j] - 48);
                                }
                                if(bars > 0 && (b[j] == 59 || b[j] == 10)) //if ; or new line
                                {
                                    if(name.length() == 0)
                                    {
                                        if(indexIncreased)
                                        {
                                            index--;
                                        }
                                        previous.id = id;
                                        end = true;
                                    }
                                    else
                                    {
                                        if(name.contentEquals("Thermi"))
                                        {
                                            name = "Deinococcus-thermus";
                                        }
                                        String scName = getScName(name.toLowerCase(), rank);
                                        if(previous.children.containsKey(scName))
                                        {
                                            previous = previous.children.get(scName);
                                        }
                                        else
                                        {
                                            Node n = new Node(scName, rank, "GG");
                                            n.nameOriginal = name;
                                            previous.putChild(scName, n);
                                            if(b[j] == 10)
                                            {
                                                n.id = id;
                                            }
                                            else
                                            {
                                                n.id = index++;
                                                indexIncreased = true;
                                            }
                                            addToNameSet(map, n);
                                            previous = n;
                                        }
                                    }
                                    name = "";
                                    rank = "";
                                }
                                else if(bars > 0 
                                    && b[j] != 34 //"
                                    && b[j] != 39 //'
                                    && b[j] != 91  //[
                                    && b[j] != 93) //]
                                {
                                    if(rank.length() == 0)
                                    {
                                        rank = tax.get(((char)b[j]) + "");
                                        j += 2;
                                    }
                                    else
                                    {
                                        name += (char) b[j];
                                    }
                                }
                            }
                        }
                    }
                    last = i;
                }
            }
        }
        IS.close();
        
        root.name = "root";
        return root;
    }

    private static Node parseSILVA(String file, Map<String, ArrayList<Node>> map) throws FileNotFoundException, IOException 
    {
        Node start = new Node("root", "root", "Silva");
        addToNameSet(map, start);
        start.nameOriginal = "SILVA";
        start.id = 1;
        
        IS = FileParser.class.getResourceAsStream(file);
        int last = 0;
        
        byte[] b = new byte[IS.available()];
        byte[] leftOver = null;
        
        int lastRead = 0;
        
        int genus = 0;
        
        HashMap<String,Integer> rankss = new HashMap<>();
        
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] == 10) // new line
                {
                    int id = 0;
                    String[] el = new String[20];
                    int index = 0;
                    String name = "";
                    String rank = "";
                    int bars = 0;
                    boolean gap = false;
                    for(int j = last; j < i; j++)
                    {
                        if(b[j] == 9) // separator "tab" found
                        {
                            bars++;
                            while(b[j+1] == 9)
                            {
                                j++;
                            }
                        }
                        else
                        {
                            if(bars == 0) //Reading path
                            {
                                if(b[j] == 59) //separator ";"
                                {
                                    el[index++] = name;
                                    
                                    name = "";
                                }
                                else if(b[j] > 32 && b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93 )
                                {
                                    if(gap && name.length() > 0)
                                    {
                                        name += " ";
                                    }
                                    gap = false;
                                    name += (char) b[j];
                                }
                            }
                            if(bars == 2) // Reading rank
                            {
                                if(b[j] > 32 && b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93 )
                                {
                                    if(gap && rank.length() > 0)
                                    {
                                        rank += " ";
                                    }
                                    gap = false;
                                    rank += (char) b[j];
                                    
                                }
                            }
                            if(bars == 1 && b[j] >= 48 && b[j] <= 57) //Reading id
                            {
                                id = id*10 + (b[j] - 48);
                            }
                            if(b[j] <= 32)
                            {
                                gap = true;
                            }
                        }
                    }
                    
                    Node previous = start;
                    
                    for (int k = 0; k < el.length; k++) 
                    {
                        if(el[k] == null)
                        {
                            break;
                        }
                       // System.out.print(l[k] + "-");
                        String nam = el[k];
                        name = nam.toLowerCase();
                        //name = getScName(name, rank);
                        Node n = previous.children.get(name);
                        if (n == null) 
                        {
                            if(el[k+1] == null)
                            {
                                n = new Node(name, rank, "Silva");
                                addToNameSet(map, n);
                            }
                            else
                            {
                                n = new Node(name, "", "Silva");
                            }
                            
                            n.nameOriginal = nam;
                            n.id = id;
                            
                            
                            previous.putChild(n.name, n);
                            n.parent = previous;
                            
                        }
                        previous = n;
                    }
                    last = i;
                }
            }
        }
        IS.close();
        
        
        
        start.type = "root";
        start.name = "root";
        return  start;   
    }

    private static Node parseOTT(String file, Map<String, ArrayList<Node>> map) throws FileNotFoundException, IOException 
    {
        Map<Integer, Node> nodes = new HashMap<>();
        Node root = null;
        
        IS = FileParser.class.getResourceAsStream(file);
        int last = 0;
        
        byte[] b = new byte[IS.available()];
        byte[] leftOver = null;
        
        int lastRead = 0;
        while(IS.available() > 0)
        {
            if(last != 0)
            {
                leftOver = Arrays.copyOfRange(b, last, lastRead);
            }
            b = new byte[IS.available()];
            int read = IS.read(b);
            
            if(leftOver != null)
            {
                byte[] tmp = new byte[leftOver.length +  read];
                System.arraycopy(leftOver, 0, tmp, 0, leftOver.length);
                System.arraycopy(b, 0, tmp, leftOver.length, read);
                b = tmp;
                read = b.length;
            }
            
            lastRead = read;
            for(int i = 0; i < read; i++)
            {
                if(b[i] == 10) // new line
                {
                    int id = 0;
                    int pId = -1;
                    String rank = "";
                    String name = "";
                    String nameLc = "";
                    String flags = "";
                    int bars = 0;
                    boolean gap = false;
                    for(int j = last; j < i; j++)
                    {
                        if(b[j] == 124) // separator "|" found
                        {
                            bars++;
                            gap = false;
                        }
                        else
                        {
                            if(bars == 0 && b[j] >= 48 && b[j] <= 57) //Reading id
                            {
                                id = id*10 + b[j] - 48;
                            }
                            if(bars == 1 && b[j] >= 48 && b[j] <= 57) //Reading parent id
                            {
                                if(pId == -1)
                                {
                                    pId = 0;
                                }
                                pId = pId*10 + b[j] - 48;
                            }
                            if(bars == 3 && b[j] >= 97 && b[j] <= 122)
                            {
                                if(gap)
                                {
                                    rank += " ";
                                    gap = false;
                                }
                                rank += (char) b[j];
                            }
                            if(bars == 2 && b[j] >= 48 && b[j] <= 122)
                            {
                                if(b[j] != 34 && b[j] != 39 && b[j] != 91 && b[j] != 93)
                                {
                                    if(gap)
                                    {
                                        name += " ";
                                        nameLc += " ";
                                        gap = false;
                                    }
                                    name += (char) b[j];
                                    if(b[j] >= 65 && b[j] <= 90)
                                    {
                                        nameLc += (char) (b[j] + 32);
                                    }
                                    else
                                    {
                                        nameLc += (char) b[j];
                                    }
                                }
                            }
                            if((bars == 2 && b[j] <= 32 && name.length() > 0)
                                    || (bars == 3 && b[j] <= 32 && rank.length() > 0))
                            {
                                gap = true;
                            }
                            if(bars == 6 && b[j] > 32)
                            {
                                flags += (char) b[j];
                            }
                        }
                    }
                    //code
                    if(!flags.contains("hidden")
                            && !flags.contains("unclassified") 
                            && !flags.contains("merged"))
                    {
                        Node n = nodes.get(id);
                        if(n == null)
                        {
                            String nam = getScName(nameLc, rank);
                            n = new Node(id, pId, nam, rank, "OTT");
                            n.nameOriginal = name;

                            nodes.put(n.id, n);
                        }
                        else
                        {
                            n.name = nameLc;
                            n.parentId = pId;
                            n.type = rank;
                            n.nameOriginal = name;
                        }
                        if(pId > -1)
                        {
                            Node parent = nodes.get(pId);
                            if(parent == null)
                            {
                                parent = new Node(pId);
                                nodes.put(pId, parent);
                            }
                            parent.putChild(n.name, n);
                            n.parent = parent;
                        }
                        if(n.name.equals("life"))
                        {
                            n.nameOriginal = "OTT";
                            n.type = "root";
                            n.name = "root";
                            root = n;
                        }
                        addToNameSet(map, n);
                    }
                    last = i;
                }
            }
        }
        IS.close();
        
        return root;
    }
    
    private static String getScName(String name, String type)
    {
        Map<String, String> synonims = getSynMap(type);
        if(synonims != null)
        {
            if(synonims.containsKey(name))
            {
                return synonims.get(name);
            }

            String extendedName = "uncultured candidate division " + name + " bacterium";
            if(synonims.containsKey(extendedName))
            {
                return synonims.get(extendedName);
            }
            if(name.contains("uncultured candidate division ") && name.contains(" bacterium"))
            {
                String sName = name;
                sName = sName.replace("uncultured candidate division ", "");
                sName = sName.replace(" bacterium", "");
                if(synonims.containsKey(sName))
                {
                    return synonims.get(sName);
                }
            }
            extendedName = "candidate division " + name;
            if(synonims.containsKey(extendedName))
            {
                return synonims.get(extendedName);
            }
            if(name.contains("candidate division "))
            {
                String sName = name;
                sName = sName.replace("candidate division ", "");
                if(synonims.containsKey(sName))
                {
                    return synonims.get(sName);
                }
            }

            extendedName = "uncultured " + name + " bacterium";
            if(synonims.containsKey(extendedName))
            {
                return synonims.get(extendedName);
            }
            if(name.contains("uncultured ") && name.contains(" bacterium"))
            {
                String sName = name;
                sName = sName.replace("uncultured ", "");
                sName = sName.replace(" bacterium", "");
                if(synonims.containsKey(sName))
                {
                    return synonims.get(sName);
                }
            }
        }
            
        return name;
    }

    private static Map<String, String> getSynMap(String type) 
    {
        switch(type)
        {
            case "phylum":
                return synonimsPhylum;
            case "class":
                return synonimsClass;
            case "order":
                return synonimsOrder;
            case "family":
                return synonimsFamily;
            case "genus":
                return synonimsGenus;
            case "species":
                return synonimsSpecies;
                        
        }
        return null;
    }
    
    private static void addToNameSet(Map<String, ArrayList<Node>> map, Node n)
    {
        if(map != null)
        {
            ArrayList<Node> sameName = map.get(n.name + "-" + n.type);
            if(sameName == null)
            {
                sameName = new ArrayList<>();
            }
            sameName.add(n);
            map.put(n.name + "-" + n.type, sameName);
        }
    }

    static JSONObject parseBiom(File inputFile,
            ArrayList<Node> biomNodes, ArrayList<JSONObject> jsonObjects,
            Node root) throws FileNotFoundException, IOException, ParseException 
    {
        Node current;
        // read the json file
        FileReader reader = new FileReader(inputFile);

        JSONParser jsonParser = new JSONParser();
        JSONObject biom = (JSONObject) jsonParser.parse(reader);

        // get a String from the JSON object

        JSONArray array = (JSONArray) biom.get("rows");
        
        
        for (int i = 0; i < array.size(); i++) 
        {
            current = root;
            JSONObject jo = (JSONObject) array.get(i);
            JSONObject metadata = (JSONObject) jo.get("metadata");
            JSONArray taxonomy = (JSONArray) metadata.get("taxonomy");
            Node n = null;
            
            for (Object taxObj : taxonomy) 
            {
                String tax = (String) taxObj;
                tax = tax.replaceAll("[\\[\\]'\"]", "");
                String[] el = tax.split("__");
                
                if(el.length == 2)
                {
                    String rank = getRank(el[0]);
                    String name = getScName(el[1].toLowerCase(), rank);
                    if(current.children.containsKey(name))
                    {
                        current = current.children.get(name);
                    }
                    else
                    {
                        n = new Node(name, rank, "BIOM");
                        current.putChild(name, n);
                        n.parent = current;
                        n.nameOriginal = el[1];
                        current = n;
                    }
                }
            }
            if(n != null)
            {
                biomNodes.add(n);
                jsonObjects.add(jo);
            }
        }
        
        return biom;
    }

    private static String getRank(String r) 
    {
        switch(r)
        {
            case "k" : return "domain";
            case "p" : return "phylum";
            case "c" : return "class";
            case "o" : return "order";
            case "f" : return "family";
            case "g" : return "genus";
            case "s" : return "species";
        }
        return null;
    }

    private static String getFile(String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}