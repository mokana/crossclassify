/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package taxonomies;

/**
 *
 * @author monika
 * @param <T>
 */
public class Parameter<T>
{
    String shortName;
    String fullName;
    String description;
    boolean show;
    boolean secret;
    T defaultValue;
    T value;

    public Parameter(String shortName, String fullName, String description, 
            T defaultValue, boolean show, boolean secret) 
    {
        this.shortName = shortName;
        this.fullName = fullName;
        this.description = description;
        this.defaultValue = defaultValue;
        this.show = show;
        this.secret = secret;
    }

}
