/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package taxonomies;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


/**
 *
 * @author monika
 */
public class Evaluator 
{
    private static CMYKColor[] flowColors;
    
    
    public Evaluator() 
    {
        flowColors = new CMYKColor[8];
        flowColors[0] = new CMYKColor(0f, 0f, 0f, 0.40f);
        flowColors[1] = new CMYKColor(0.07f, 0f, 0.30f, 0f);
        flowColors[2] = new CMYKColor(0.22f, 0f, 0.27f, 0f);
        flowColors[3] = new CMYKColor(0.50f, 0f, 0.20f, 0f);
        flowColors[4] = new CMYKColor(0.75f, 0f, 0.10f, 0f);
        flowColors[5] = new CMYKColor(0.90f, 0.15f, 0f, 0f);
        flowColors[6] = new CMYKColor(0.90f, 0.45f, 0f, 0f);
        flowColors[7] = new CMYKColor(1f, 0.70f, 0f, 0.10f);
    }
    
    public void makeStats(Node treeToMap, Node refTree,
            ArrayList<Node> biomNodes,
            File parallelSetsFile, File heatmapFile,
            Boolean paths) throws DocumentException, IOException
    {
        double maxPenalty = 0.0;
        double penalty = 0.0;

        double penLin = 0.0;
        double maxPenLin = 0.0;
        double all = 0.0;


        int[][] mapped = new int[8][8];
        int[][] refMap = new int[8][8];

        ArrayList<Node> nodesToMap = new ArrayList<>();
        
        if(biomNodes == null || biomNodes.isEmpty())
        {
            nodesToMap.add(treeToMap);
        }
        else
        {
            nodesToMap.addAll(biomNodes);
        }

        while(!nodesToMap.isEmpty())
        {
            Node n = nodesToMap.remove(0);
            String domain = n.getDomain();
  
            if(n.print && ((paths && n.inPath) || !paths))
            {
                if(biomNodes == null || biomNodes.isEmpty())
                {
                    nodesToMap.addAll(n.children.values());
                }

                Node m = n.getM();

                int mtype = getClosestType(m);
                int ntype = getClosestType(n);

                n.mappedTo = ntype - mtype;

                if(ntype != 7)
                {
                    int cntype = ntype == 7 ? 6 : ntype;
                    int cmtype = mtype == 7 ? 6 : mtype;

                    if(cmtype != cntype)
                    {
                        if(cmtype == 0)
                        {
                            penalty += power(2.0, 5);
                            penLin += (double)(cntype);
                        }
                        else
                        {
                            penalty += power(2.0, Math.abs(cntype-cmtype)-1);
                            penLin += (double)Math.abs(cntype - cmtype);
                        }
                    }
                    maxPenalty += power(2.0, 5);
                    maxPenLin += (double) cntype;
                    all += 1.0;
                }

                mapped[ntype][mtype]++;
                if(!m.mapped[ntype])
                {
                    refMap[mtype][ntype]++;
                    m.mapped[ntype] = true;
                } 
            }
        }

        //printMappingStats(mapped);
        
        if(parallelSetsFile != null)
        {
            saveParallelSets(mapped, refMap, parallelSetsFile, 
                    treeToMap.source, refTree.source);
        }
        double[][] mappedNorm = normalise(mapped);


        double score = penalty/maxPenalty;
        double linSc = penLin/all;
        if(heatmapFile != null)
        {
            saveAHeatMap(mappedNorm, heatmapFile, treeToMap.source, refTree.source);
        }
        System.out.println("Normed mapping distance " + treeToMap.source 
                + " to " + refTree.source + ": " + (penLin/maxPenLin));
    }
    
    private int getClosestType(Node n)
    {
        int type = 0;
        String rank = n.type;
        if(rank.startsWith("sub"))
        {
            rank = rank.substring(3);
        }
        else if(rank.startsWith("super") || rank.startsWith("infra"))
        {
            rank = rank.substring(5);
        }
        if(n.type != null && Info.hi.containsKey(rank))
        {
            type =  Info.hi.get(rank);
        }
        else
        {
            LinkedList<Node> nPath = n.getPath();
            while(!nPath.isEmpty())
            {
                Node nn = nPath.removeLast();
                
                rank = nn.type;
                if(rank.startsWith("sub"))
                {
                    rank = rank.substring(3);
                }
                else if(rank.startsWith("super") || rank.startsWith("infra"))
                {
                    rank = rank.substring(5);
                }
                
                if(nn.type != null && Info.hi.containsKey(rank))
                {
                    type =  Info.hi.get(rank);
                    break;
                }
            }
        }
        return type;
    }
    
    static double power(double d, int p)
    {
        double r;
        if(p == 0)
        {
            r = 0.0;
        }
        else
        {
            r = 1.0;
            for (int i = 0; i < p; i++) 
            {
                r *= d;
            }
        }
        return r;
    }
    
    static double[][] normalise(int[][] mapped)
    {
        int m = mapped.length;
        int[] all = new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                all[i] += mapped[i][j];
            }
        }
        
        double[][] mappedNorm = new double[m][m];
        for (int i = 0; i < m; i++) 
        {
            for (int j = 0; j < m; j++) 
            {
                mappedNorm[i][j] = ((double)mapped[i][j])/((double)all[i]);
            }
        }
        
        return mappedNorm;
    }
        
    private void printMappingStats(int[][] mapped) throws DocumentException, FileNotFoundException, IOException
    {
        String output = "Mapping statistics:\n";
        
        for (int i = 0; i < mapped.length; i++)
        {
            //System.out.print("\t" + ih.get(i));
            output += "\t" + Info.ih.get(i);
            
        }
        //System.out.println("");
        output += "\n";
        for (int i = 0; i < mapped.length; i++)
        {
            //System.out.print(ih.get(i));
            output += Info.ih.get(i);
            for (int j = 0; j < mapped[i].length; j++) 
            {
                //System.out.print("\t" + mapped[i][j]);
                output += "\t" + mapped[i][j];
            }
            //System.out.println("");
            output += "\n";
        }
        
        System.out.print(output);
    }
    
    void saveParallelSets(int[][] mapped, int[][] refMap, 
            File file, String in, String ref) throws DocumentException, IOException
    {            
        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        double[] toMapComp = getCompositionStats(mapped);
        double[] refComp = getCompositionStats(refMap);
        
        float offset = 3;
        float extraTo = 50;//bf.getWidthPoint(Integer.toString(maxTo), 10);
        float extraRef = 50;//bf.getWidthPoint(Integer.toString(maxRef), 10);
        float w = 10;
        float gap = 100;
        
        float textSize = 10;
        
        float sp = 2;
        float spTop = 7;
        
        float windowHeight = 175 + 6*sp + textSize + spTop;
        float windowWidth = extraTo + offset + w + gap + w + offset + extraRef;
        
        double[][] toTable = new double[8][2];
        double[][] refTable = new double[8][2];
                

        Document pdf = new Document(new Rectangle(windowWidth, windowHeight));
        PdfWriter writer = PdfWriter.getInstance(pdf, new FileOutputStream(file));
        pdf.open();
        
        float y1 = windowHeight;
        float y2 = windowHeight;
        float x1 = extraTo + offset;
        float x2 = windowWidth - extraRef - offset - w;
        
        float h = 5;
        
        PdfContentByte cb = writer.getDirectContent();

        cb.setColorStroke(BaseColor.BLACK);
        
        addTaxonomyLabel(cb, bf, textSize, in, x1 + w/2, y1);
        
        addTaxonomyLabel(cb, bf, textSize, ref, x2 + w/2, y2);
        
        y1 -= (textSize + spTop);
        y2 -= (textSize + spTop);
        
        for (int i = 1; i < toMapComp.length; i++)
        {
            //int h1 = (int)((double)toMapComp[i]*(double)size/(double)sToMap);
            toTable[i][0] = y1;
            h+= 5;
            toTable[i][1] = h;
            
            
            Rectangle rec1 = new Rectangle(x1, y1, x1+w, y1-h);
            rec1.setBackgroundColor(flowColors[i]);
            y1 -= (h+sp);
            
            //int h2 = (int)((double)refComp[i]*(double)size/(double)sRef);
            refTable[i][0] = y2;
            refTable[i][1] = h;
            
            Rectangle rec2 = new Rectangle(x2, y2, x2+w, y2-h);
            rec2.setBackgroundColor(flowColors[i]);
            y2 -= (h+sp);
            
            pdf.add(rec1);
            pdf.add(rec2);
                        
            cb.beginText();
            cb.setFontAndSize(bf, 10);
            String num = Integer.toString((int) toMapComp[i]);
            float textWidth = bf.getWidthPoint(num, 10);
            cb.moveText(x1 - offset - textWidth, y2 + h/2 - 3+sp);
            cb.showText(num);
            cb.endText();
            
            cb.beginText();
            cb.setFontAndSize(bf, 10);
            num = Integer.toString((int) refComp[i]);
            cb.moveText(x2 + w + offset, y2 + h/2 - 3+sp);
            cb.showText(num);
            cb.endText();
            
        }
        
        PdfGState gState = new PdfGState();
        gState.setFillOpacity(0.5f);
        cb.setGState(gState);
                
        for (int i = 1; i < toMapComp.length; i++)
        {
            float yTo = (float) toTable[i][0];
            float sum = (float) toMapComp[i];
            

            for (int j = 1; j < mapped[i].length; j++)
            {
                float sum2 = (float) refComp[j];
                int m = mapped[i][j];
                int mRef = refMap[j][i];
                if(m != 0)
                {
                    float h1 = ((float)m*(float)toTable[i][1]/(float)sum);
                    
                    float yRef = (float) refTable[j][0];

                    
                    float h2 = ((float)mRef*(float)refTable[j][1]/(float)sum2);
                    
                    CMYKColor co = flowColors[i];
                    cb.setColorFill(co);
                    
                    cb.moveTo(x1+w, yTo);
                    cb.lineTo(x2, yRef);
                    cb.lineTo(x2, yRef-h2);
                    cb.lineTo(x1+w, yTo-h1);
                    cb.fill();
                    
                    yTo -= h1;
                }
            }
        }
        pdf.close();
    }

    private void addTaxonomyLabel(PdfContentByte cb, BaseFont bf, float textSize, String text, float xPos, float y2) {
        cb.beginText();
        cb.setFontAndSize(bf, textSize);
        float textWidth = bf.getWidthPoint(text, textSize);
        cb.moveText(xPos - textWidth/2, y2 - textSize);
        cb.showText(text);
        cb.endText();
    }

    private double[] getCompositionStats(int[][] matrix) 
    {
        double[] stats  = new double[8];
        for (int i = 0; i < 8; i++)
        {
            stats[i] = sum(matrix[i]);
        }
        return stats;
    }
    
    private int sum(int[] array)
    {
        int sum = 0;
        for(double i : array)
        {
            sum += i;
        }
        return sum;
    }
    
    private static void saveAHeatMap(double[][] matrix, File heatmapFile,
            String in, String ref) throws DocumentException, FileNotFoundException, IOException
    {
        float cellH = 12;
        float cellW = 24;
        
        float textSize = 10;
        float textWidth = 30;
        float sp = 5;
        float spTop = 7;
        
        float offset = 12;
        
        float n = matrix.length;
        float windowWidth = n * cellW + offset + textWidth + spTop;
        float windowHeight = n * cellH + offset + textSize + spTop;
        Document pdf = new Document(new Rectangle(windowWidth, windowHeight));
        PdfWriter writer = PdfWriter.getInstance(pdf, new FileOutputStream(heatmapFile));
        pdf.open();
        
        PdfContentByte cb = writer.getDirectContent();

        cb.setColorStroke(BaseColor.BLACK);
        
        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        String[] legend = new String[]{"R","D","P","C","O","F","G", "S"};
        
        float labelW = bf.getWidthPoint(ref, textSize);
        
        float y = windowHeight - textSize;
        float x = textWidth + sp + offset + (n * cellW)/2 - labelW;
        
        placeText(cb, bf, x, y, ref);
        
        labelW = bf.getWidthPoint(in, textSize);
        x = textWidth - labelW;
        y = (windowHeight - textSize - sp - offset - textSize) /2;
        
        placeText(cb, bf, x, y, in);
        
        y = windowHeight - textSize - sp - offset;
        x = offset + textWidth + sp;
        
        for (int i = 0; i < legend.length; i++) 
        {
            placeText(cb, bf, x + cellW/2-4, y+2, legend[i]);
            x += cellW;
        }
        
        for (int i = 0; i < matrix.length; i++)
        {
            x = textWidth + sp;
            
            placeText(cb, bf, x, y-cellH+2, legend[i]);
            
            x += offset;
            
            for (int j = 0; j < matrix[i].length; j++) 
            {
                Rectangle rec = new Rectangle(x, y, x+cellW, y-cellH);
                //Color c = Color.getHSBColor(0f, (float)matrix[i][j], 1);

                
                rec.setBackgroundColor(new CMYKColor((float)matrix[i][j],(float)(matrix[i][j]/2.0), 0f,  0));
                pdf.add(rec);
                        
                cb.beginText();
                cb.setFontAndSize(bf, 10);
                Double val = matrix[i][j];
                if(val.isNaN())
                {
                    val = 0.0;
                }
                String num = String.format( "%.2f", val);
                cb.moveText(x+2, y-cellH+2);
                cb.showText(num);
                cb.endText();
                
                x += cellW;

            }
            
            y -= cellH;
            
        }
        
        pdf.close();
    }

    private static void placeText(PdfContentByte cb, BaseFont bf, float x, float y, String ref) 
    {
        cb.beginText();
        cb.setFontAndSize(bf, 10);
        cb.moveText(x, y);
        cb.showText(ref);
        cb.endText();
    }

    static void printNoRank(Map<String, ArrayList<Node>> toMap) 
    {
        int all = 0;
        int noRank = 0;
        int intermediateRank = 0;
        for(ArrayList<Node> list : toMap.values())
        {
            for(Node n : list)
            {
                all++;
                if(n.type.contains("no rank"))
                {
                    noRank++;
                }
                else if(!Info.hi.containsKey(n.type))
                {
                    intermediateRank++;
                    System.out.println(n.type);
                }
            }
        }
        double noR = (double)noRank/(double)all;
        double intR = (double)intermediateRank/(double)all;
        System.out.println("No rank\t" + noR);
        System.out.println("Int rank\t" + intR);
        System.out.println("Not clear\t" + (noR+intR));
    }
    
    static void printSize(Map<String, ArrayList<Node>> map, String tax) throws IOException
    {
        ArrayList<String>[] ranks = new ArrayList[8];
        for (int i = 0; i < ranks.length; i++) {
            ranks[i] = new ArrayList<>();
        }
        int norank = 0;
        
        int i = 0;
        for(ArrayList<Node> set : map.values())
        {
            i += set.size();
            for(Node n : set)
            {
                if(Info.hi.containsKey(n.type))
                {
                    ranks[Info.hi.get(n.type)].add(n.name);
                }
                else if(n.type.equals("no rank"))
                {
                    norank++;
                }
            }
        }
        System.out.println("#Nodes " + i);
        System.out.println("No rank " + norank);
        
        
        for (int j = 7; j < 8; j++) {
            
            String fn = tax + "_" + Info.ih.get(j) + ".list";
            BufferedWriter br = new BufferedWriter(new java.io.FileWriter(fn));
            
            System.err.println(Info.ih.get(j) + "\t" + ranks[j].size());
            for(String name : ranks[j])
            {
                br.write(name + "\n");
            }
            br.close();
        }
    }
    
}


