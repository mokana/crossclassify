/*
 * CrossClassify is a tool for mapping results of metagenomic analyses from one taxonomy onto another.
 * Copyright (C) 2016 Monika Balvociute
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package taxonomies;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author monika
 */
public class Parameters 
{
    static String[] args;
    static Map<String, Parameter> pars = new HashMap<>();
    static ArrayList<String> parlist = new ArrayList<>();
    

    static String getString(String abrv, String full, 
            String description, String defaultValue,
            boolean show, boolean secret) throws Exception 
    {
        Parameter<String> p = new Parameter<>(abrv, full, description, defaultValue, show, secret);
        pars.put(abrv, p);
        
        String value = get(args, abrv, full, true);
                
        if(value == null)
        {
            value = defaultValue;
        }
        p.value = value;
        
        return value;
    }
    
    static File getFile(String abrv, String full, 
            String description, String defaultValue,
            boolean show, boolean secret) throws Exception 
    {
        Parameter<String> p = new Parameter<>(abrv, full, description, defaultValue, show, secret);
        pars.put(abrv, p);
        
        String value = get(args, abrv, full, true);
                
        if(value == null)
        {
            value = defaultValue;
        }
        
        File file = null;
        if(value != null)
        {
            file = new File(value);
            p.value = file.getCanonicalPath();
        }
        
        return file;
    }
    
    
    static Boolean getBoolean(String abrv, String full, 
            String description, Boolean defaultValue,
            boolean show, boolean secret) throws Exception 
    {
        Parameter<Boolean> p = new Parameter<>(abrv, full, description, defaultValue, show, secret);
        pars.put(abrv, p);
        
        String val = get(args, abrv, full, false);
        Boolean value;
                
        if(val != null && val.contentEquals("true"))
        {
            value = true; 
        }
        else
        {
            value =  defaultValue;
        }
        p.value = value;
        return value;
    }
    
    
    
    private static String get(String[] args, 
            String abrv, String full, 
            boolean hasValue) throws Exception
    {
        String value = null;
        parlist.add(abrv);
        int no = 0;
        
        for (int i = 0; i < args.length; i++) 
        {
            if(args[i].contentEquals(abrv) || args[i].contentEquals(full))
            {
                no++;
                if(no > 1)
                {
                    throw new Exception("Parameter " + abrv + "/" + full + " set multiple times");
                }
                
                if(hasValue && args.length == i+1)
                {
                    throw new Exception("Missing value for the parameter " + abrv + "/" + full);
                }
                
                if(hasValue)
                {
                    value = args[i+1].toLowerCase();
                }
                else
                {
                    value = "true";
                }
            }
        }
        return value;
    }

    static void printHelp() 
    {
        for(String name : parlist)
        {
            Parameter p = pars.get(name);
            if(!p.secret)
            {
                System.out.print(p.shortName + "/" + p.fullName + " \t" + p.description);
                if(p.defaultValue != null)
                {
                    System.out.print(" Default " + p.defaultValue.toString() + ".");
                }
                System.out.println("");
            }
        }
    }

    static void printValues() throws IOException 
    {
        LocalDateTime time = LocalDateTime.now();
        String timeStamp = time.getDayOfMonth() + "/" +
                time.getMonth().getValue() + "/" +
                time.getYear() + " " +
                time.getHour()+":"+time.getMinute()+":"+time.getSecond() + " " +
                ZoneId.systemDefault().toString();
        System.out.println(timeStamp);
        
        for(String name : parlist)
        {
            Parameter p = pars.get(name);
            if(p.show && !p.secret && (p.value != null || p.defaultValue != null))
            {
                System.out.print(p.shortName + "/" + p.fullName + " \t");
                if(p.value != null)
                {
                    System.out.println(p.value.toString());
                }
                else
                {
                    System.out.println(p.defaultValue.toString());
                }
            }
        } 
        System.out.println("");
    }
}
